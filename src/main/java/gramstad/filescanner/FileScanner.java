package gramstad.filescanner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used for gathering information about a directory and its subdirectories.
 */
public class FileScanner {

    static ScanResult result;

    //Main entrypoint, takes in a directory path and returns information about that directory
    public static ScanResult scan(String path) throws Exception {

         result = new ScanResult();

        File root = new File(path);

        if(!root.exists()){
            throw new Exception("Directory: "+path+" does not exist");
        }

        processDirectory(root);

        return result;
    }

    //Recursive method to gather information about a directory and its content
    private static void processDirectory(File inputFile){

        if (inputFile.isFile()){
            result.files.add(inputFile);
            return;
        }else if (inputFile.isDirectory()){
            result.directoryCount++;
        }

        for(File subFile : inputFile.listFiles()){
            processDirectory(subFile);
        }
    }

    //holds the information collected about target directory
    public static class ScanResult {

        public int directoryCount = 0;
        public List<File> files = new ArrayList<File>();

        public ScanResult(){
            directoryCount = 0;
            files = new ArrayList<File>();
        }

        //Returns number of files that were found in the directory
        public int getNumFiles(){
            return files.size();
        }

        //Returns number of directories under the parent directory
        public int getNumDirectories(){
            return directoryCount;
        }

        //Returns the total size of all
        public long getTotalBytes(){

            long length = 0;

            for(File file: files){
                length = length + file.length();
            }
            return length;
        }

        //returns avg size of files in directory and sub directories.
        public double getAvgBytes(){
            //cast to double for more precision
            return ((double) getTotalBytes() / files.size());
        }


    }

}

