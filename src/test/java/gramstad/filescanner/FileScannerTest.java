package gramstad.filescanner;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.PrintWriter;

/**
 * class used for testing FileScanner
 */
@RunWith(JUnit4.class)
public class FileScannerTest {

    //convenience method used to clean up after tests
    private void recursiveDelete(File toDelete){

        if(toDelete.isFile()){
            toDelete.delete();
            return;
        }

        for(File subFile : toDelete.listFiles()){
            recursiveDelete(subFile);
        }

        toDelete.delete();
    }

    //example of how to use external directory structure
    @Ignore
    @Test
    public void exampleWithHardPath() throws Exception {

        String path = "some path to directory";

        FileScanner.ScanResult result = FileScanner.scan(path);

        //validate results here
    }

    /**
     * Tests below create a directory structure for various scenarios, validate the scanner is working correctly
     * then tear down the directory structure that was created.
     */

    @Test
    public void singleDirectory() throws Exception {

        File root = new File("./testroot1");
        root.mkdirs();

        FileScanner.ScanResult result = FileScanner.scan(root.getPath());

        Assert.assertEquals(1, result.getNumDirectories());
        Assert.assertEquals(0, result.getNumFiles());
        Assert.assertEquals(0, result.getTotalBytes());

        recursiveDelete(root);
    }


    @Test
    public void singleFile() throws Exception {

        File root = new File("./testroot2");
        //create a test directory
        root.createNewFile();

        PrintWriter pr = new PrintWriter(root.getPath());
        pr.println("Testing String");
        pr.close();

        FileScanner.ScanResult result = FileScanner.scan(root.getPath());

        Assert.assertEquals(0, result.getNumDirectories());
        Assert.assertEquals(1, result.getNumFiles());
        Assert.assertEquals(15, result.getTotalBytes());
        Assert.assertEquals(15.0, result.getAvgBytes(), 0);

        recursiveDelete(root);
    }

    @Test
    public void doubleDirectory() throws Exception {

        File root = new File("./testroot3");
        root.mkdirs();

        File subdir = new File("./testroot3/subdir");
        subdir.mkdirs();

        String testPath = "";

        FileScanner.ScanResult result = FileScanner.scan(root.getPath());

        Assert.assertEquals(2, result.getNumDirectories());
        Assert.assertEquals(0, result.getNumFiles());
        Assert.assertEquals(0, result.getTotalBytes());

        recursiveDelete(root);
    }

    @Test
    public void singledir_doublefile() throws Exception {

        File root = new File("./testroot2");
        root.mkdirs();

        File root21 = new File("./testroot2/testroot21");
        root21.createNewFile();

        File root22 = new File("./testroot2/testroot22");
        root22.createNewFile();

        PrintWriter pr = new PrintWriter(root21.getPath());
        pr.println("Testing String");
        pr.close();

        PrintWriter pr2 = new PrintWriter(root22.getPath());
        pr2.println("Testing String");
        pr2.close();

        FileScanner.ScanResult result = FileScanner.scan(root.getPath());

        Assert.assertEquals(1, result.getNumDirectories());
        Assert.assertEquals(2, result.getNumFiles());
        Assert.assertEquals(30, result.getTotalBytes());
        Assert.assertEquals(15.0, result.getAvgBytes(), 0);

        recursiveDelete(root);
    }

    @Test
    public void doubledir_doublefile() throws Exception {

        File root = new File("./testroot7");
        root.mkdirs();

        File root1 = new File("./testroot7/testroot5");
        root1.mkdirs();

        File root2 = new File("./testroot7/testroot6");
        root2.mkdirs();

        File root21 = new File("./testroot7/testroot5/testroot21");
        root21.createNewFile();

        File root22 = new File("./testroot7/testroot22");
        root22.createNewFile();

        PrintWriter pr = new PrintWriter(root21.getPath());
        pr.println("Testing String");
        pr.close();

        PrintWriter pr2 = new PrintWriter(root22.getPath());
        pr2.println("Testing String");
        pr2.close();

        FileScanner.ScanResult result = FileScanner.scan(root.getPath());

        Assert.assertEquals(3, result.getNumDirectories());
        Assert.assertEquals(2, result.getNumFiles());
        Assert.assertEquals(30, result.getTotalBytes());
        Assert.assertEquals(15.0, result.getAvgBytes(), 0);

        recursiveDelete(root);
    }

}
